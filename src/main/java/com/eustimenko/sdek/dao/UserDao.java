package com.eustimenko.sdek.dao;

import com.eustimenko.sdek.model.User;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface UserDao {

    Long insertUser(String name);

    List<User> findByName(String name) throws DataAccessException;

    User findById(long id) throws DataAccessException;

    List<User> findAll() throws DataAccessException;
}
