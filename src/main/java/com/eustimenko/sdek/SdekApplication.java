package com.eustimenko.sdek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdekApplication {

    public static void main(String[] args) {
        SpringApplication.run(SdekApplication.class, args);
    }
}
