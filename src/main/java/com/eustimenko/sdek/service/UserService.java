package com.eustimenko.sdek.service;

import com.eustimenko.sdek.model.User;

import java.util.List;

public interface UserService {

    User addUser(String name);

    List<User> list();

    List<User> list(String name);
}
